Source: gnome-shell-extension-tiling-assistant
Section: gnome
Priority: optional
Maintainer: Debian GNOME Maintainers <pkg-gnome-maintainers@lists.alioth.debian.org>
Uploaders: Fabio Augusto De Muzio Tobich <ftobich@debian.org>, @GNOME_TEAM@
Build-Depends: debhelper-compat (= 13),
               dh-sequence-gnome,
               gettext
Standards-Version: 4.6.1
Homepage: https://extensions.gnome.org/extension/3733/tiling-assistant/
Vcs-Browser: https://salsa.debian.org/gnome-team/shell-extensions/gnome-shell-extension-tiling-assistant
Vcs-Git: https://salsa.debian.org/gnome-team/shell-extensions/gnome-shell-extension-tiling-assistant.git
Rules-Requires-Root: no

Package: gnome-shell-extension-tiling-assistant
Architecture: all
Depends: ${misc:Depends},
         gnome-shell (>= 43~),
         gnome-shell (<< 45~)
Recommends: gnome-shell-extension-prefs
Description: extension which adds a Windows-like snap assist to GNOME Shell
 An extension which adds a Windows-like snap assist to GNOME Shell.
 .
 It also expands GNOME's 2 column tiling design and adds more features, like:
  * Tiling Popup: This is the popup, which shows up when a window is tiled and
    there is an (unambiguous) free screen rectangle. It lists all open windows
    on the current workspace. Activating one of the popup's icons will tile the
    window to fill the remaining screen space.
  * Tile Groups: Tiled windows are considered in a group, if they don't overlap
    each other and aren't interrupted by non-tiled windows. If one of the
    windows is focused, the rest of the group will be raised to the foreground
    as well. A Tile Group also resizes together.
  * Layouts: A layout is a list of arbitrary rectangles. When activating one
    with its keybinding the Tiling Popup asks you which of the open windows you
    want at which spot of your layout. The layout selector is a popup, which
    will lists all defined layouts by name. This way you don't have to remember
    the layouts' keybindings.
  * Pie Menu: Super + RMB on a window will open a simple pie menu.
